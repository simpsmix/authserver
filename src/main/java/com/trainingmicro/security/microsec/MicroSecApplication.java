package com.trainingmicro.security.microsec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroSecApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroSecApplication.class, args);
	}

}

